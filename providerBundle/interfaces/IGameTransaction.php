<?php

namespace providerBundle\interfaces;

/**
 * Interface IGameTransaction
 * @package providerBundle\interfaces
 */
interface IGameTransaction
{
    /**
     * Find transaction.
     * Return Integrator's transaction ID on success or FALSE on failure.
     *
     * @param string $providerId
     * @param string $action
     * @param string $gameSessionId
     *
     * @throws \Exception
     * @return false|string
     */
    function find($providerId, $action, $gameSessionId);

    /**
     * Find BET transaction which has not yet been refunded.
     * Return TRUE on success or Integrator's transaction ID of REFUND transaction on failure.
     *
     * @param string $providerBetId
     * @param string $gameSessionId
     *
     * @throws \Exception
     * @return string|true
     */
    function findSuccessBet($providerBetId, $gameSessionId);

    /**
     * Create new transaction.
     * Return new Integrator's transaction ID
     *
     * @param $providerId
     * @param $action
     * @param $gameSessionId
     * @param bool $success {optional} Default TRUE. If incorrect transaction, then FALSE.
     *
     * @throws \Exception
     * @return string
     */
    function create($providerId, $action, $gameSessionId, $success = true);

    /**
     * Deny BET transaction after REFUND.
     *
     * @param string $providerBetId
     * @param string $gameSessionId
     *
     * @throws \Exception
     */
    function denyBet($providerBetId, $gameSessionId);
}
